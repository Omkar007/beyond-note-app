package syr.beyondnoteapp.beyondnote;

import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Database {

    public static class UserData {
        public String email;
        public String imagePath;
    }

    public static class NotesData implements Serializable {
        public String id;
        public String title;
        public String content;
        public String date;
        public String time;
        public String color;
        public String tags;
        public String images;

        NotesData() {
            id = "";
            title = "";
            content = "";
            date = "";
            time = "";
            color = "";
            tags = "";
            images = "";
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getTags() {
            return tags;
        }

        public void setTags(String tags) {
            this.tags = tags;
        }

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }
    }

    public static class TaskData implements Serializable {
        public String id;
        public String title;
        public String description;
        public String priority;
        public String date;
        public String time;
        public String tags;
        public String location;
        public String contacts;

        TaskData()
        {
            id = "";
            title = "";
            description = "";
            priority = "";
            date = "";
            time = "";
            tags = "";
            location = "";
            contacts = "";
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPriority() {
            return priority;
        }

        public void setPriority(String priority) {
            this.priority = priority;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getTags() {
            return tags;
        }

        public void setTags(String tags) {
            this.tags = tags;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getContacts() {
            return contacts;
        }

        public void setContacts(String contacts) {
            this.contacts = contacts;
        }

        @Override
        public String toString() {
            return "TaskData{" +
                    "id='" + id + '\'' +
                    ", title='" + title + '\'' +
                    ", description='" + description + '\'' +
                    ", priority='" + priority + '\'' +
                    ", date='" + date + '\'' +
                    ", time='" + time + '\'' +
                    ", tags='" + tags + '\'' +
                    ", location='" + location + '\'' +
                    ", contacts='" + contacts + '\'' +
                    '}';
        }
    }

    /* Member Variable */
    DatabaseReference mDatabaseRef;
    UserData thisUserData;
    public List<Map<String,String>> mNotesList;
    public List<Map<String,String>> mTaskList;
    public List<Map<String,String>> mTagsList;

    /* Member Functions */
    public Database(String EmailId) {
        thisUserData = new UserData();
        if(EmailId != null) {
            mDatabaseRef = FirebaseDatabase.getInstance().getReference().child(getUserIdFromEmail(EmailId)).getRef();
            Log.d("initDatabaseEmail", EmailId);
        }
        mNotesList = new ArrayList<>();
        mTaskList = new ArrayList<>();
        mTagsList = new ArrayList<>();
    }

    //Replace '.' with '_' as '.' is invalid path for root tag
    protected static String getUserIdFromEmail(String Email) {

        String UserId = Email;
        return UserId.replace('.','_');
    }

    public static void addUserProfile(UserData NewUserData) {
        DatabaseReference NewDbRef = FirebaseDatabase.getInstance().getReference();
        Log.d("Database:addUserProfile", NewUserData.toString());

        if(NewUserData.email != null) {
            NewDbRef.child(getUserIdFromEmail(NewUserData.email)).setValue(NewUserData);
        } else {
            Log.e("email is null", "");
        }
    }

    public void addNoteToDatabase(NotesData MyNoteData) {
        Log.d("Database:addNote : ", MyNoteData.toString());

        //Generate Unique Key
        if(MyNoteData.id.equals("")) {
            MyNoteData.id = mDatabaseRef.push().getKey();
        }

        //Add data in Cloud
        mDatabaseRef.child("Note").child(MyNoteData.id).setValue(MyNoteData);
    }

    public void removeNoteFromDatabase(NotesData MyNoteData) {
        Log.d("Database:removeNote : ", MyNoteData.toString());

        if(MyNoteData.id != null) {
            mDatabaseRef.child("Note").child(MyNoteData.id).removeValue();
        } else {
            Log.e("id is null", "");
        }
    }

    public void addTaskToDatabase(TaskData MyTaskData) {
        Log.d("Database:addTask : ", MyTaskData.toString());

        //Generate Unique Key
        if(MyTaskData.id.equals("")) {
            MyTaskData.id  = mDatabaseRef.push().getKey();
        }

        //Add data in Cloud
        mDatabaseRef.child("Task").child(MyTaskData.id).setValue(MyTaskData);
    }

    public void removeTaskFromDatabase(TaskData MyTaskData) {
        Log.d("Database:removeTask : ", MyTaskData.toString());

        if(MyTaskData.id != null) {
            mDatabaseRef.child("Task").child(MyTaskData.id).removeValue();
        } else {
            Log.e("id is null", "");
        }
    }

    public void initializeUserProfileFromCloud()
    {
        mDatabaseRef.addChildEventListener(new com.google.firebase.database.ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.getKey().equals("email")) {
                    thisUserData.email = (String) dataSnapshot.getValue();
                } else if (dataSnapshot.getKey().equals("imagePath")) {
                    thisUserData.imagePath = (String) dataSnapshot.getValue();
                }
            }
            @Override public void onChildChanged(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {}
            @Override public void onChildRemoved(com.google.firebase.database.DataSnapshot dataSnapshot) {}
            @Override public void onChildMoved(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {}
            @Override public void onCancelled(DatabaseError databaseError) {}
        });
    }

    public static void TestDb()
    {
        Database.UserData NewUser = new Database.UserData();
        NewUser.email = "abc@xyz.com";
        NewUser.imagePath = "www.dummyImage.com";
        Database.addUserProfile(NewUser);

        Database MyDb = new Database(NewUser.email);
        Database.NotesData MyNote = new Database.NotesData();
        MyNote.title="MyNote1";
        MyNote.content="MyContent1";
        MyDb.addNoteToDatabase(MyNote);
        //MyDb.removeNoteFromDatabase(MyNote);

        Database.TaskData MyTask = new Database.TaskData();
        MyTask.title="MyTask1";
        MyTask.description="MyDescription1";
        MyDb.addTaskToDatabase(MyTask);
        //MyDb.removeTaskFromDatabase(MyTask);
    }

    protected int addDataToList(List<Map<String,String>> MyList, HashMap NewData)
    {
        int insertPosition = 0;
        String NewDataId = (String)NewData.get("id");

        for (int i = 0; i < MyList.size(); i++) {
            HashMap Data = (HashMap)MyList.get(i);
            String DataId = (String)Data.get("id");

            if (DataId.equals(NewDataId)) {
                return 0;
            }

            if (DataId.compareTo(NewDataId) > 0) {
                insertPosition = i + 1;
            } else {
                break;
            }
        }
        MyList.add(insertPosition, NewData);
        Log.d("addDataToList", NewData.toString());

        return insertPosition;
    }

    protected void updateDataInList(List<Map<String,String>> MyList, HashMap UpdatedData)
    {
        String UpdatedDataId = (String) UpdatedData.get("id");
        for(int i = 0; i < MyList.size(); i++)
        {
            HashMap Data = (HashMap)MyList.get(i);
            String DataId = (String)Data.get("id");
            if (DataId.equals(UpdatedDataId))
            {
                MyList.remove(i);
                MyList.add(i, UpdatedData);
                Log.d("updateDataInList", UpdatedData.toString());
                break;
            }
        }
    }

    protected void removeDataFromList(List<Map<String,String>> MyList, HashMap removeData)
    {
        int position = -1;
        String removeDataId=(String)removeData.get("id");

        for (int i = 0; i < MyList.size(); i++)
        {
            HashMap data= (HashMap)MyList.get(i);
            String dataId = (String)data.get("id");
            if(dataId.equals(removeDataId))
            {
                position = i;
                break;
            }
        }

        if (position != -1)
        {
            MyList.remove(position);
            Log.d("removeDataFromList", removeData.toString());
        }
    }


    HashMap<String, String> getNotesMap(NotesData MyNote) {
        HashMap<String, String> myMap = new HashMap<>();
        Field[] allFields = NotesData.class.getFields();
        for (Field field : allFields) {
            Class<?> targetType = field.getType();
            try {
                if(field.get(MyNote) instanceof String) {
                    myMap.put(field.getName(), (String) field.get(MyNote));
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return myMap;
    }

    HashMap<String, String> getTaskMap(TaskData MyTask) {
        HashMap<String, String> myMap = new HashMap<>();
        Field[] allFields = TaskData.class.getFields();
        for (Field field : allFields) {
            Class<?> targetType = field.getType();
            try {
                if(field.get(MyTask) instanceof String) {
                    myMap.put(field.getName(), (String) field.get(MyTask));
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return myMap;
    }

    public void initializeDataFromCloud()
    {
        mNotesList.clear();
        mTaskList.clear();
        mTagsList.clear();

        Log.d("initDatabase", "adding listners");

        mDatabaseRef.addChildEventListener(new com.google.firebase.database.ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                //Log.d("Database:OnChildAdded", dataSnapshot.toString());

                if(dataSnapshot.getKey().equals("Note")) {
                    Log.d("Note Children :", String.valueOf(dataSnapshot.getChildrenCount()));
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        //Log.d("Child ", child.toString());
                        NotesData MyNote = child.getValue(NotesData.class);
                        //Log.d("Id = ", MyNote.id);
                        HashMap<String, String> MyNoteMap = getNotesMap(MyNote);
                        //Log.d("Map ID : ", MyNoteMap.get("id"));
                        addDataToList(mNotesList, MyNoteMap);
                        addDataToList(mTagsList, MyNoteMap);
                    }
                }

                else if(dataSnapshot.getKey().equals("Task")) {
                    Log.d("Task Children :", String.valueOf(dataSnapshot.getChildrenCount()));
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        //Log.d("Child ", child.toString());
                        TaskData MyTask = child.getValue(TaskData.class);
                        //Log.d("Id = ", MyTask.id);
                        HashMap<String, String> MyTaskMap = getTaskMap(MyTask);
                        //Log.d("Map ID : ", MyTaskMap.get("id"));
                        addDataToList(mTaskList, MyTaskMap);
                        addDataToList(mTagsList, MyTaskMap);
                    }
                }
            }

            @Override
            public void onChildChanged(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {
                //Log.d("Database:OnChildChanged", dataSnapshot.toString());

                if(dataSnapshot.getKey().equals("Note")) {
                    Log.d("Note Children :", String.valueOf(dataSnapshot.getChildrenCount()));
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        //Log.d("Child ", child.toString());
                        NotesData MyNote = child.getValue(NotesData.class);
                        //Log.d("Id = ", MyNote.id);
                        HashMap<String, String> MyNoteMap = getNotesMap(MyNote);
                        //Log.d("Map ID : ", MyNoteMap.get("id"));
                        updateDataInList(mNotesList, MyNoteMap);
                        updateDataInList(mTagsList, MyNoteMap);
                        addDataToList(mNotesList, MyNoteMap);
                        addDataToList(mTagsList, MyNoteMap);
                    }
                }

                if(dataSnapshot.getKey().equals("Task")) {
                    Log.d("Task Children :", String.valueOf(dataSnapshot.getChildrenCount()));
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        //Log.d("Child ", child.toString());
                        TaskData MyTask = child.getValue(TaskData.class);
                        //Log.d("Id = ", MyTask.id);
                        HashMap<String, String> MyTaskMap = getTaskMap(MyTask);
                        //Log.d("Map ID : ", MyTaskMap.get("id"));
                        updateDataInList(mTaskList, MyTaskMap);
                        updateDataInList(mTagsList, MyTaskMap);
                        addDataToList(mTaskList, MyTaskMap);
                        addDataToList(mTagsList, MyTaskMap);
                    }
                }
            }

            @Override
            public void onChildRemoved(com.google.firebase.database.DataSnapshot dataSnapshot) {
                //Log.d("Database:OnChildRemoved", dataSnapshot.toString());

                if(dataSnapshot.getKey().equals("Note")) {
                    Log.d("Note Children :", String.valueOf(dataSnapshot.getChildrenCount()));
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        //Log.d("Child ", child.toString());
                        NotesData MyNote = child.getValue(NotesData.class);
                        //Log.d("Id = ", MyNote.id);
                        HashMap<String, String> MyNoteMap = getNotesMap(MyNote);
                        //Log.d("Map ID : ", MyNoteMap.get("id"));
                        removeDataFromList(mNotesList, MyNoteMap);
                        removeDataFromList(mTagsList, MyNoteMap);
                    }
                }

                if(dataSnapshot.getKey().equals("Task")) {
                    Log.d("Task Children :", String.valueOf(dataSnapshot.getChildrenCount()));
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        //Log.d("Child ", child.toString());
                        TaskData MyTask = child.getValue(TaskData.class);
                        //Log.d("Id = ", MyTask.id);
                        HashMap<String, String> MyTaskMap = getTaskMap(MyTask);
                        //Log.d("Map ID : ", MyTaskMap.get("id"));
                        removeDataFromList(mTaskList, MyTaskMap);
                        removeDataFromList(mTagsList, MyTaskMap);
                    }
                }
            }

            @Override
            public void onChildMoved(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
