package syr.beyondnoteapp.beyondnote;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.transition.Slide;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;

import com.firebase.ui.auth.ui.User;

import java.util.HashMap;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.ScaleInAnimator;

public class Notes_Fragment_RecyclerView extends Fragment {

    private static final String ARG_USER_ID = "UserID";
    String UserId;
    static NotesDatabase MyNoteDb;
    RecyclerView rv;
    NotesRecyclerViewFirebaseAdapter NotesRvFirebaseAdapter;
    notesEventHandler myEventHandlers;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment Notes_Fragment_RecyclerView.
     */
    public static Notes_Fragment_RecyclerView newInstance(String UserId) {
        Notes_Fragment_RecyclerView fragment = new Notes_Fragment_RecyclerView();

        Bundle args = new Bundle();
        args.putString(ARG_USER_ID, UserId);
        fragment.setArguments(args);

        return fragment;
    }

    public Notes_Fragment_RecyclerView() {
        // Required empty public constructor
    }

    public interface notesEventHandler
    {
        public void onFabClick();
        public void configureActionBar();
    }

    public void setEventHandlers(notesEventHandler myEventHandlers)
    {
        this.myEventHandlers = myEventHandlers;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            UserId = (String) getArguments().getString(ARG_USER_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.notes_fragment_recyler_view, container, false);

        //Initialize Recycler View
        rv = (RecyclerView) rootView.findViewById(R.id.recyclerViewNotes);
        rv.setHasFixedSize(true);

        //Set Layout
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        //Create Database Instance
        if(null == MyNoteDb) {
            MyNoteDb = new NotesDatabase(UserId);
            //MyNoteDb.initializeDataFromCloud();

        }

        //Set Adapter
        //InitializeNotesRvAdapter();
        InitializeNotesRvFirebaseAdapter();


        // Initialize FAB to add new note
        //todo callback from activity
        FloatingActionButton myFab = (FloatingActionButton)  rootView.findViewById(R.id.notes_FAB_add_note);
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //Load fragment to construct new note
                loadMyNoteFragment(new Database.NotesData());
            }
        });
        defaultanimation();
        adapteranimation();
        itemanimation();
        return rootView;
    }

    void loadMyNoteFragment(Database.NotesData MyNote)
    {
        Notes_Fragment_Add addNoteFragment = Notes_Fragment_Add.newInstance(MyNote);
        addNoteFragment.setEventCallback(new Notes_Fragment_Add.doEventCallback() {
            @Override
            public void onNoteCreate(Database.NotesData MyNoteData) {

                /* Add Note to Cloud Database */
                MyNoteDb.addNoteToDatabase(MyNoteData);

                /* Scroll to new data when screen comes back */
                //NewNotePosition = Position;
            }

            @Override
            public void onNoteUpdate(Database.NotesData MyNoteData) {
                MyNoteDb.addNoteToDatabase(MyNoteData);
            }

            @Override
            public void configureActionBar() {
                if(myEventHandlers != null)
                    myEventHandlers.configureActionBar();
            }
        });

        //addNoteFragment.setEnterTransition(new Slide(Gravity.RIGHT));
        //addNoteFragment.setExitTransition(new Slide(Gravity.LEFT));
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container_main, addNoteFragment)
                .addToBackStack(null)   //Ensure screen comes up when back is clicked from new note
                .commit();

//        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(
//                new FragmentManager.OnBackStackChangedListener() {
//                    public void onBackStackChanged() {
//                        int backStackEntryCount = getActivity().getSupportFragmentManager().getBackStackEntryCount();
//                        //Toast.makeText(getActivity(), "BackStack : " + backStackEntryCount, Toast.LENGTH_SHORT).show();
//
//                        //Go back to original screen
//                        // Update your UI here
//                        if(backStackEntryCount == 0) {
//                            //todo Scroll to new note when added
//                            //rv.getLayoutManager().scrollToPosition(NewNotePosition);
//                        }
//                    }
//                });
    }

//    void InitializeNotesRvAdapter()
//    {
//        NotesRvAdapter = new NotesRecyclerViewAdapter (getActivity(), MyNoteDb.getNotesList());
//        rv.setAdapter(NotesRvAdapter);
//
//        NotesRvAdapter.setOnNotesItemClickListener(new NotesRecyclerViewAdapter.OnNotesItemClickListener() {
//
//            @Override
//            public void onNoteItemClick(View v, int position) {
//                Toast.makeText(getActivity(), "Click Position : " + position, Toast.LENGTH_SHORT).show();
//
//                //Load Note Detail Fragment
////                Fragment_Movie MyMovieFrag = Fragment_Movie.newInstance(position, mv);
////                AppCompatActivity activity = (AppCompatActivity) v.getContext();
////                activity.getSupportFragmentManager().beginTransaction()
////                        .replace(R.id.frag_container, MyMovieFrag)
////                        .addToBackStack(null)
////                        .commit();
//            }
//
//            @Override
//            public void onNoteItemLongClick(View v, int position) {
//                Toast.makeText(getActivity(), "Long Click Position : " + position, Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    void InitializeNotesRvFirebaseAdapter()
    {
        NotesRvFirebaseAdapter = new NotesRecyclerViewFirebaseAdapter (Database.NotesData.class,
                    R.layout.notes_card_view,
                    NotesRecyclerViewFirebaseAdapter.NotesViewHolder.class,
                    MyNoteDb.mDatabaseRef
        );

        rv.setAdapter(NotesRvFirebaseAdapter);

        NotesRvFirebaseAdapter.setOnNotesItemClickListener(new NotesRecyclerViewFirebaseAdapter.OnNotesItemClickListener() {

            @Override
            public void onNoteItemClick(View v, int position) {
                //Load Note Detail Fragment
                loadMyNoteFragment(NotesRvFirebaseAdapter.getItem(position));
            }

            @Override
            public void onNoteItemLongClick(View v, int position) {
                Toast.makeText(getActivity(), "Long Click Position : " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNoteHamburgerIconClick(View view, int position) {
                ShowPopupMenu(view, position);
            }
        });
    }

    void ShowPopupMenu(View view, final int position)
    {
        Context myContext = new ContextThemeWrapper(getActivity(), R.style.AppTheme);
        PopupMenu popup = new PopupMenu(myContext, view);

        popup.getMenuInflater().inflate(R.menu.notes_options_menu, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                switch(item.getItemId())
                {
                    case R.id.note_option_delete:
                        MyNoteDb.removeNoteFromDatabase(NotesRvFirebaseAdapter.getItem(position));
                        break;
                    case R.id.note_option_duplicate:
                        Database.NotesData DuplicateNote = NotesRvFirebaseAdapter.getItem(position);

                        DuplicateNote.setId("");
                        DuplicateNote.setTitle(DuplicateNote.getTitle());
                        DuplicateNote.setDate(Utility.GetData());
                        DuplicateNote.setTime(Utility.GetTime());

                        MyNoteDb.addNoteToDatabase(DuplicateNote);
                        break;
                }
                return true;
            }
        });
        popup.show(); //showing popup menu
    }
    private  void  defaultanimation()
    {

        DefaultItemAnimator animator=new DefaultItemAnimator();
        animator.setAddDuration(3000);
        animator.setRemoveDuration(100);
        rv.setItemAnimator(animator);
    }

    private void adapteranimation()
    {

        AlphaInAnimationAdapter alphaInAnimationAdapter=new AlphaInAnimationAdapter(NotesRvFirebaseAdapter);
        ScaleInAnimationAdapter scaleInAnimationAdapter=new ScaleInAnimationAdapter(alphaInAnimationAdapter);
        rv.setAdapter(scaleInAnimationAdapter);
    }

    private void itemanimation()
    {
        ScaleInAnimator animator=new ScaleInAnimator();
        animator.setInterpolator(new OvershootInterpolator());
        animator.setAddDuration(300);
        animator.setRemoveDuration(300);
        rv.setItemAnimator(animator);
    }
}
