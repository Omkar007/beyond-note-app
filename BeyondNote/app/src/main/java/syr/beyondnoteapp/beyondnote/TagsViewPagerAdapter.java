package syr.beyondnoteapp.beyondnote;

import android.nfc.TagLostException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class TagsViewPagerAdapter extends FragmentPagerAdapter {

    List<Map<String, String>> myTagsList;
    Map<String, List<Map<String, String>>> TagsMap;
    List<String> TagsList;
    int count;

    public TagsViewPagerAdapter(FragmentManager fm, List<Map<String, String>> aTagsList)
    {
        super(fm);
        myTagsList = aTagsList;

        TagsMap = new HashMap<>();
        TagsList = new ArrayList<>();
        count = myTagsList.size();

        UpdateTagsList();
    }

    @Override
    public Fragment getItem(int position) {
        String Tag = TagsList.get(position);
        Log.d("getItem", Tag + " : " + position);
        List<Map<String, String>> NoteTaskList = TagsMap.get(Tag);
        return TagsRecyclerViewFragment.newInstance(NoteTaskList);
    }

    @Override
    public int getCount() {
        UpdateTagsList();
        Log.d("getCount", "" + TagsList.size());
        return TagsList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Log.d("getPageTitle", TagsList.get(position) + " : " + position);
        return TagsList.get(position);
    }

    void UpdateTagsList()
    {
        if(count == myTagsList.size())
            return;

        for(Map<String, String> myMap : myTagsList) {
            String[] Tags = ((String)myMap.get("tags")).split(",");
            for(String tag : Tags) {
                Log.d("updatefor", tag);
                List<Map<String, String>> NoteTaskList = TagsMap.get(tag);
                if(NoteTaskList == null) {
                    NoteTaskList = new ArrayList<>();
                    TagsMap.put(tag, NoteTaskList);
                    TagsList.add(tag);
                }

                //Check if already exist?
                if(!NoteTaskList.contains(myMap)) {
                    NoteTaskList.add(myMap);
                    notifyDataSetChanged();
                }
            }
        }

        count = myTagsList.size();
    }
}
