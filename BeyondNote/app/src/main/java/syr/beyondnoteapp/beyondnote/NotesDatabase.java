package syr.beyondnoteapp.beyondnote;

import android.util.Log;

import com.google.firebase.database.FirebaseDatabase;


public class NotesDatabase extends Database {

    String DbNoteTag = "Note";

    NotesDatabase(String EmailId) {
        super(EmailId);

        if(EmailId != null) {
            Log.d("NoteDb:Email", EmailId);
            mDatabaseRef = FirebaseDatabase.getInstance().getReference().child(getUserIdFromEmail(EmailId)).child(DbNoteTag).getRef();
        }
    }

    @Override
    public void addNoteToDatabase(NotesData MyNoteData) {
        Log.d("NotesDatabase:addNote", MyNoteData.toString());

        //Generate Unique Key
        if(MyNoteData.id.equals("")) {
            MyNoteData.id = mDatabaseRef.push().getKey();
        }

        //Add data in Cloud
        mDatabaseRef.child(MyNoteData.id).setValue(MyNoteData);
    }

    @Override
    public void removeNoteFromDatabase(NotesData MyNoteData) {
        Log.d("NoteDatabase:removeNote", MyNoteData.toString());

        if(MyNoteData.id != null) {
            mDatabaseRef.child(MyNoteData.id).removeValue();
        } else {
            Log.e("id is null", "");
        }
    }

//    @Override
//    public void initializeDataFromCloud()
//    {
//        mNotesList.clear();
//
//        mDatabaseRef.addChildEventListener(new com.google.firebase.database.ChildEventListener() {
//
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                Log.d("NoteDb:OnChildAdded", dataSnapshot.toString());
//                NotesData MyNote = dataSnapshot.getValue(NotesData.class);
//                Log.d("Id = ", MyNote.id);
//                HashMap<String, String> MyNoteMap = getNoteHashMap(MyNote);
//                //Log.d("Map ID : ", MyNoteMap.get("id"));
//                addDataToList(mNotesList, MyNoteMap);
//            }
//
//            @Override
//            public void onChildChanged(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {
//                Log.d("NoteDb:OnChildChanged", dataSnapshot.toString());
//                NotesData MyNote = dataSnapshot.getValue(NotesData.class);
//                Log.d("Id = ", MyNote.id);
//                HashMap<String, String> MyNoteMap = getNoteHashMap(MyNote);
//                //Log.d("Map ID : ", MyNoteMap.get("id"));
//                updateDataInList(mNotesList, MyNoteMap);
//            }
//
//            @Override
//            public void onChildRemoved(com.google.firebase.database.DataSnapshot dataSnapshot) {
//                Log.d("Database:OnChildRemoved", dataSnapshot.toString());
//                NotesData MyNote = dataSnapshot.getValue(NotesData.class);
//                Log.d("Id = ", MyNote.id);
//                HashMap<String, String> MyNoteMap = getNoteHashMap(MyNote);
//                //Log.d("Map ID : ", MyNoteMap.get("id"));
//                removeDataFromList(mNotesList, MyNoteMap);
//            }
//
//            @Override
//            public void onChildMoved(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//    }
}
