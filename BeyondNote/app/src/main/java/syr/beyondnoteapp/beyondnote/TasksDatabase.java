package syr.beyondnoteapp.beyondnote;

import android.util.Log;

import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;

public class TasksDatabase extends Database implements Serializable{

    String DBTaskTag = "Task";

    TasksDatabase(String EmailId) {
        super(EmailId);

        if(EmailId != null) {
            Log.d("TaskDB:Email", EmailId);
            mDatabaseRef = FirebaseDatabase.getInstance().getReference().child(getUserIdFromEmail(EmailId)).child(DBTaskTag).getRef();
        }
    }

    @Override
    public void addTaskToDatabase(TaskData myTaskData) {
        Log.d("TaskDatabase:addTask", myTaskData.toString());

        //Generate Unique Key
        if(myTaskData.id.equals("")) {
            myTaskData.id = mDatabaseRef.push().getKey();
        }

        //Add data in Cloud
        mDatabaseRef.child(myTaskData.id).setValue(myTaskData);
    }

    @Override
    public void removeTaskFromDatabase(TaskData myTaskData) {
        Log.d("TaskDatabase:removeTask", myTaskData.toString());

        if(myTaskData.id != null) {
            mDatabaseRef.child(myTaskData.id).removeValue();
        } else {
            Log.e("id is null", "");
        }
    }

}
