package syr.beyondnoteapp.beyondnote;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.squareup.picasso.Picasso;

import android.Manifest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class AddTaskFragment extends Fragment {
    private static final int PLACE_PICKER_REQUEST = 1;
    private static final String TASK_USER_ID = "UserID";
    private static final String ADD_TASK_DB = "TASKDB";
    int priorityImg[]=new int[]{R.drawable.silverstarimg,R.drawable.goldstartimg,R.drawable.redstarpr};

    // Request code for READ_CONTACTS. It can be any number > 0.
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;

    CharSequence ActionBarTitle;
    EditText date;
    EditText description;
    EditText time;
    EditText taskLocation;
    EditText taskContacts;
    String userId;
    TasksDatabase taskDB;
    ImageView priorityImage;
    private ListView lstNames;
    private SwipeRefreshLayout swipeContainer;
    int resID=-1;

    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));

    public AddTaskFragment(){
    }

    /* Add interface to allow calling activity/fragment to communicate */
    public interface eventTaskCallback
    {
        //Pass the generated note to calling activity/fragment
        //It would want to save it in database or display to user
        void onTaskSubmit(Database.TaskData myTaskData);
    }
    AddTaskFragment.eventTaskCallback eventCallback;

    public static AddTaskFragment newInstance(String UserId,TasksDatabase myTaskDB) {
        AddTaskFragment fragment = new AddTaskFragment();
        Bundle args = new Bundle();
        args.putString(TASK_USER_ID, UserId);
        args.putSerializable(ADD_TASK_DB,myTaskDB);
        fragment.setArguments(args);
        return fragment;
    }
    /* allow calling activity/fragment to implement interface */
    public void setEventCallback(AddTaskFragment.eventTaskCallback customCallback) {
        eventCallback = customCallback;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userId = (String) getArguments().getString(TASK_USER_ID);
            taskDB= (TasksDatabase) getArguments().get(ADD_TASK_DB);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {

        View view = null;
        view = inflater.inflate(R.layout.activity_add_task, container,
                false);

        //Set action Bar title
        ActionBar actionbar = ((TaskRecyclerView) getActivity()).getSupportActionBar();
        ActionBarTitle = actionbar.getTitle();
        actionbar.setTitle("Add New Task");

        date = (EditText) view.findViewById(R.id.task_add_date);
        time = (EditText) view.findViewById(R.id.task_add_time);
        taskLocation = (EditText) view.findViewById(R.id.add_location_task);
        description = (EditText) view.findViewById(R.id.add_task_description);
        priorityImage = (ImageView) view.findViewById(R.id.addProirityImg);
        taskContacts = (EditText) view.findViewById(R.id.add_contacts_task);

        //priority Image listener
        priorityImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              getPriorityResourceID();
            }
        });

        //date picker event handler
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooseDate();
            }
        });

        //time picker event handler
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooseTime();
            }
        });

        //maps event handler
        taskLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    PlacePicker.IntentBuilder intentBuilder =
                            new PlacePicker.IntentBuilder();
                    intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
                    Intent intent = intentBuilder.build(getActivity());
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);

                } catch (GooglePlayServicesRepairableException
                        | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        taskContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Read and show the contacts
                showContacts();
            }
        });

        return view;
    }

    //Function to open Date Picker dialog
    public void ChooseDate(){
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String datePicked = String.valueOf(year) +"-"+String.valueOf(monthOfYear)
                        +"-"+String.valueOf(dayOfMonth);
                date.setText(datePicked);
            }
        }, yy, mm, dd);
        datePicker.show();
    }

    //Function to open Time Picker dialog
    public void ChooseTime(){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                time.setText( selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    //Activity overrided function that call the Google Map Place Piccker and stores the place
    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode, Intent data) {

        if (requestCode == PLACE_PICKER_REQUEST
                && resultCode == Activity.RESULT_OK) {

            final Place place = PlacePicker.getPlace(getContext(), data);
            final CharSequence name = place.getName();
            final CharSequence address = place.getAddress();
            String attributions = (String) place.getAttributions();
            if (attributions == null) {
                attributions = "";
            }
            String location=name+","+address+","+Html.fromHtml(attributions);
            taskLocation.setText(location);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    //Generating object query to persist in database
    public Database.TaskData getTaskData(){

        Database.TaskData taskTobeSaved = new Database.TaskData();
        taskTobeSaved.title=((EditText)getView().findViewById(R.id.add_task_title)).getText().toString();
        taskTobeSaved.date=((EditText)getView().findViewById(R.id.task_add_date)).getText().toString();
        taskTobeSaved.time=((EditText)getView().findViewById(R.id.task_add_time)).getText().toString();
        taskTobeSaved.location=((EditText)getView().findViewById(R.id.add_location_task)).getText().toString();
        taskTobeSaved.description=((EditText)getView().findViewById(R.id.add_task_description)).getText().toString();
        taskTobeSaved.priority=String.valueOf(resID);
        taskTobeSaved.contacts=((EditText)getView().findViewById(R.id.add_contacts_task)).getText().toString();

        taskTobeSaved.tags=getTags();

        return  taskTobeSaved;
    }

    //get tags from description
    private String getTags()
    {
        String content = ((TextView)getView().findViewById(R.id.add_task_description)).getText().toString();
        String tags = "";

        int start;
        for (int i = 0 ; i < content.length() ; i++)
            if (content.charAt(i) == '#') {
                i++;    //Skip #
                start = i;
                while(i < content.length() && Character.isLetterOrDigit(content.charAt(i)))
                    i++;    //continue till end of word
                if(start != i)
                    tags += content.substring(start, i) + ",";
            }

        //remove "," at end of string
        if(tags.length() > 0)
            return tags.substring(0, tags.length() - 1);
        else
            return "";
    }

    //saves to database when fragment gets destroyed
    @Override
    public void onDestroyView() {

        if(!description.getText().toString().equals("Enter Content")) {      //hard coded default value
            //save in db
            Toast.makeText(getActivity(), "Saved Task", Toast.LENGTH_SHORT).show();
//            if(eventCallback != null) {
//                //calling back the calling fragment
//                eventCallback.onTaskSubmit(getTaskData());
//            }
            taskDB.addTaskToDatabase(getTaskData());
            //Added for SQL DB content provider
            ADDTaskContentDB();
        }
        super.onDestroyView();
    }

    //get the drawablw resource id of priority image
public int getPriorityResourceID(){
    resID++;
    Log.d("pr count",String.valueOf(resID));
    priorityImage.setImageResource(0);
    if(resID>2){
        resID=0;
    }
if(resID==0){
    priorityImage.setImageResource(priorityImg[resID]);
}
else if(resID==1){
    priorityImage.setImageResource(priorityImg[resID]);
}
else if(resID==2){
    priorityImage.setImageResource(priorityImg[resID]);
}
    return resID;
}

    /**
     * Show the contacts in the ListView.
     */
    private void showContacts() {

        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getContext().checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {
            // Android version is lesser than 6.0 or the permission is already granted.
            List<String> contacts = getContactNames();
            populateDialog(contacts);
        }
    }

    // populating contacts dialog box
    public void populateDialog(List<String> contacts){
        final Dialog dialog = new Dialog(getActivity(),R.style.question_dialog);
        dialog.setContentView(R.layout.contacts_dialog);
        dialog.setTitle(" Add Contacts! ");
        swipeContainer = (SwipeRefreshLayout) dialog.findViewById(R.id.swipeContainer);
        lstNames = (ListView)dialog.findViewById(R.id.lstNames);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                (getContext(), android.R.layout.simple_list_item_multiple_choice, contacts){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                // Get the Item from ListView
                View view = super.getView(position, convertView, parent);

                // Initialize a TextView for ListView each Item
                TextView tv = (TextView) view.findViewById(android.R.id.text1);

                // Set the text color of TextView (ListView Item)
                tv.setTextColor(Color.BLACK);
                // Generate ListView Item using TextView
                return view;
            }
        };
        lstNames.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        lstNames.setAdapter(arrayAdapter);

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try{
                    arrayAdapter.clear();
                    getContactNames();
                    arrayAdapter.notifyDataSetChanged();
                    swipeContainer.setRefreshing(false);
                }
                catch (Exception e){
                    System.out.println("Exception occured while refreshing "+e.getMessage());
                }
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        final Button dialogCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);;

        dialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SparseBooleanArray checked = lstNames.getCheckedItemPositions();
                String str="";
                for (int i = 0; i < checked.size(); i++) {
                    // Item position in adapter
                    int position = checked.keyAt(i);
                    // Add sport if it is checked i.e.) == TRUE!
                    if (checked.valueAt(i))
                        str = str +arrayAdapter.getItem(position)+",";
                }
                taskContacts.setText(str);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                showContacts();
            } else {
                Toast.makeText(getContext(), "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Read the name of all the contacts.
     *
     * @return a list of names.
     */
    private List<String> getContactNames() {
        List<String> contacts = new ArrayList<>();
        // Get the ContentResolver
        ContentResolver cr = getContext().getContentResolver();
        // Get the Cursor of all the contacts
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        // Move the cursor to first. Also check whether the cursor is empty or not.
        if (cursor.moveToFirst()) {
            // Iterate through the cursor
            do {
                // Get the contacts name
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if(name.contains("@")){
                    String formatName = name.substring(0,name.indexOf('@'));
                    contacts.add(formatName);
                }
                else{
                    contacts.add(name);
                }
            } while (cursor.moveToNext());
        }
        // Close the curosor
        cursor.close();

        return contacts;
    }

    public void ADDTaskContentDB() {
        // Add a new task record
        ContentValues values = new ContentValues();
        values.put(TaskProvider.Title,
                ((EditText)getView().findViewById(R.id.add_task_title)).getText().toString());

        values.put(TaskProvider.Description,
                ((EditText)getView().findViewById(R.id.add_task_description)).getText().toString());

        Uri uri = getContext().getContentResolver().insert(
                TaskProvider.CONTENT_URI, values);

  /*      Toast.makeText(getContext(),
                uri.toString(), Toast.LENGTH_LONG).show(); */
    }
}
