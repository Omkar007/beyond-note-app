package syr.beyondnoteapp.beyondnote;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class Activity_Main extends AppCompatActivity
    implements NavigationView.OnNavigationItemSelectedListener {

    Toolbar toolbar;
    NavigationView navigationView;
    DrawerLayout drawerLayout;
    String UserId;
    TextView userName,Email;
    private SpeechRecognizer mSpeechRecognizer;
    private Intent mSpeechRecognizerIntent;
    private static final String TASK_USER_ID = "UserID";
    private StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //returned intent that started this activity
        if(savedInstanceState!= null) {
            UserId=savedInstanceState.getString(TASK_USER_ID);
        }
        else{
            Intent intentExtras = getIntent();
            UserId = intentExtras.getStringExtra("email");

        }
      //  Log.d("User Id ", UserId);
        storageReference= FirebaseStorage.getInstance().getReference();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                downLoadFile(UserId);
            }
        }, 2000);

        //Initializing toolbar and setting it as action bar
        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        //Setup Navigation Drawer
        SetupNavigationDrawer();

        //Load main page fragment
        manageNotes();

//        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
//        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
//        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
//                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
//        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
//                this.getPackageName());
//
//
//        SpeechRecognitionListener listener = new SpeechRecognitionListener();
//        mSpeechRecognizer.setRecognitionListener(listener);
//        mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
    }

    void SetupNavigationDrawer()
    {
        //Initializing Navigation View
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Initializing Drawer Layout and ActionBarToggle
        drawerLayout = (DrawerLayout) findViewById(R.id.layout_drawer_main);
//        userName = (TextView) navigationView.findViewById(R.id.user_name);
//        Email = (TextView) navigationView.findViewById(R.id.email_id);
//
//        if(UserId != null){
//            Email.setText(UserId);
//            userName.setText(UserId.substring(0,UserId.indexOf('@')));
//        }
        ActionBarDrawerToggle actionBarDrawerToggle =
                new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {

                    /** Called when a drawer has settled in a completely open state. */
                    public void onDrawerOpened(View drawerView) {
                        super.onDrawerOpened(drawerView);
                        //getSupportActionBar().setTaskTitle("Select UserProfile!");
                        //invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                    }

                    /* Called when a drawer has settled in a completely closed state. */
                    public void onDrawerClosed(View view) {
                        super.onDrawerClosed(view);
                        //getSupportActionBar().setTaskTitle("Test Title");
                        //invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                    }
                };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        //Icons appearing grey fix
        //http://stackoverflow.com/questions/31394265/navigation-drawer-item-icon-not-showing-original-colour
        navigationView.setItemIconTintList(null);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        Intent intent;

        Bundle bundle = new Bundle();
        bundle.putString("email",UserId);

        int id = item.getItemId();

        switch(id) {
            case R.id.item1_notes:
                //Load Notes Fragment
                manageNotes();
                break;
            case R.id.item2_tasks:
                //Load Tasks Activity
                Intent myIntent = new Intent(Activity_Main.this, TaskRecyclerView.class);
                myIntent.putExtras(bundle);
                startActivity(myIntent);
                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                break;
            case R.id.item3_tags:
                //Load Tags Activity
                intent = new Intent(Activity_Main.this, Activity_Tag.class);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.item4_userinfo:
                //Load User Info using Twitter API Oauth Functionality
                Intent userIntent = new Intent(Activity_Main.this, TwitterRetrofitActviity.class);
                startActivity(userIntent);
                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                break;
            case R.id.item5_logout:
                FirebaseAuth auth = FirebaseAuth.getInstance(); auth.signOut();
                intent = new Intent(this,LoginActivity.class);
                startActivity(intent);
            default:
                //Load Notes Activity
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    void manageNotes()
    {
        Notes_Fragment_RecyclerView FragNotesRv = Notes_Fragment_RecyclerView.newInstance(UserId);

        FragNotesRv.setEventHandlers(new Notes_Fragment_RecyclerView.notesEventHandler() {
            @Override
            public void onFabClick() {

            }

            @Override
            public void configureActionBar() {
                ConfigureAppBar();
            }
        });

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container_main, FragNotesRv)
                .commit();
    }

    void ConfigureAppBar()
    {
        // Fix for toolbar remains hidden when changing fragment.
        // When the Fragment gets replaced, the position for the Toolbar remains the same, i.e., if the Toolbar is hidden, it will stay so which isn't intended.
        //http://stackoverflow.com/questions/30554824/how-to-reset-the-toolbar-position-controlled-by-the-coordinatorlayout

        CoordinatorLayout coordinator = (CoordinatorLayout) findViewById(R.id.main_coord_layout);
        AppBarLayout appbar = (AppBarLayout) findViewById(R.id.main_app_bar_layout);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appbar.getLayoutParams();
        AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) params.getBehavior();
        int[] consumed = new int[2];
        behavior.onNestedPreScroll(coordinator, appbar, null, 0, -1000, consumed);

        //Animation
        //behavior.onNestedFling(coordinator, appbar, null, 0, -1000, true);
    }

    public void downLoadFile(String userId){
        StorageReference riversRef = storageReference.child("images/").child(userId + ".jpg");
        riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {

                Uri downloadUri = uri;
                Log.d("image path",uri.toString());
                Picasso.with(Activity_Main.this).load(uri).noPlaceholder().centerCrop().fit()
                        .into((ImageView) findViewById(R.id.profile_image));
                // The string(file link) that you need
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
              //  Toast.makeText(getApplicationContext(),"failed to retrieve Image",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TASK_USER_ID,UserId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSpeechRecognizer != null)
        {
            mSpeechRecognizer.destroy();
        }

    }

    public class SpeechRecognitionListener implements RecognitionListener
    {

        @Override
        public void onBeginningOfSpeech()
        {
            System.out.println("speaking");
        }

        @Override
        public void onBufferReceived(byte[] buffer)
        {
        }

        @Override
        public void onEndOfSpeech()
        {

        }

        @Override
        public void onError(int error)
        {
            mSpeechRecognizer.startListening(mSpeechRecognizerIntent);

            //Log.d(TAG, "error = " + error);
        }

        @Override
        public void onEvent(int eventType, Bundle params)
        {

        }

        @Override
        public void onPartialResults(Bundle partialResults)
        {

        }

        @Override
        public void onReadyForSpeech(Bundle params)
        {
            Toast.makeText(getApplicationContext(),"Voice Navigation Ready to Start!",Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onResults(Bundle results)
        {
            //Log.d(TAG, "onResults"); //$NON-NLS-1$
            ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            // matches are the return values of speech recognition engine
            System.out.println(matches.get(0));
        }

        @Override
        public void onRmsChanged(float rmsdB)
        {
        }
    }
}
