package syr.beyondnoteapp.beyondnote;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

public class TaskRecyclerView extends AppCompatActivity {
    TextView title;
    String UserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_recycler_view);

        //returned intent that started this activity
        Intent intentExtras = getIntent();
        UserId = intentExtras.getStringExtra("email");

        //toolbar for sort, search
        Toolbar toolbar = (Toolbar) findViewById(R.id.task_action_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        title = (TextView) findViewById(R.id.tvToolBarTitle);
        title.setText("Tasks List");

    // Replacing framelayout of task with recycler view fragment
        getSupportFragmentManager().beginTransaction().
                add(R.id.container2, TaskRecyclerFragment.newInstance(R.id.taskrecyclerFrag,UserId),"TagRecycleforAddTask")
                .commit();
    }


}
