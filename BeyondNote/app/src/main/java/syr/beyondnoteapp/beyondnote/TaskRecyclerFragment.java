package syr.beyondnoteapp.beyondnote;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.ScaleInAnimator;

public class TaskRecyclerFragment extends Fragment {

    //default constructor required
    public TaskRecyclerFragment() {
    }
    /* Add interface to allow calling activity/fragment to communicate */
//    public interface floatBarTaskCallback
//    {
//        //Pass the generated note to calling activity/fragment
//        //It would want to save it in database or display to user
//        void AddTaskFragmentToDB();
//    }
//    TaskRecyclerFragment.floatBarTaskCallback taskEventCall;

    private static  final String ARG_Section_number ="section_number";
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    TaskRVAdapter taskFirebaseRecylerAdapter;
    TasksDatabase myTaskDB;
    static String userId;
    FloatingActionButton addtask;

//    /* allow calling activity/fragment to implement interface */
//    public void floatBarTaskCallback(TaskRecyclerFragment.floatBarTaskCallback customCallback) {
//        taskEventCall = customCallback;
//    }

    public static  TaskRecyclerFragment newInstance(int sectionNumber,String UserId)
    {
        TaskRecyclerFragment fragment=new TaskRecyclerFragment();
        Bundle args=new Bundle();
        args.putInt(ARG_Section_number,sectionNumber);
        fragment.setArguments(args);
        userId=UserId;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=null;
        view = inflater.inflate(R.layout.task_recycler_fragment, container,false);

        //intializing database
        if(myTaskDB==null){
            myTaskDB=new TasksDatabase(userId);
        }

        //add task
        addtask=(FloatingActionButton) view.findViewById(R.id.addTaskButton);
        addtask.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AddNewTask();
               // AddTaskFragmentToDB();
            }
        });

        //calling TaskFirebase Adapter constructor
        taskFirebaseRecylerAdapter=new TaskRVAdapter(Database.TaskData.class,R.layout.itemlayout,TaskRVAdapter.TaskViewHolder.class,
                myTaskDB.mDatabaseRef,getContext());
        recyclerView = (RecyclerView)
                view.findViewById(R.id.recycler_view2) ;
        recyclerView.setHasFixedSize(true);
        layoutManager = new
                LinearLayoutManager(view.getContext());   // set layout manager
        recyclerView.setLayoutManager(layoutManager);

        //connecting the TaskRecyclerFirebase Adapter
        recyclerView.setAdapter(taskFirebaseRecylerAdapter);

//        TaskRecyclerView activity= (TaskRecyclerView) getActivity();
//        activity.addtask.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                AddTaskFragmentToDB();
//            }
//        });
        //implmenting ItemClick Listener
        taskFirebaseRecylerAdapter.setOnItemClickListener(new TaskRVAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Toast.makeText(getActivity(), taskFirebaseRecylerAdapter.getItem(position).title, Toast.LENGTH_SHORT).show();
                Database.TaskData taskMap=taskFirebaseRecylerAdapter.getItem(position);
                getActivity().getSupportFragmentManager().beginTransaction().
                        replace(R.id.container2, TaskFragment.newInstance(taskMap)).addToBackStack(null)
                        .commit();
            }

            @Override
            public void onItemLongClick(View view, int position) {
                Toast.makeText(getActivity(), "Long Click Position : " + position, Toast.LENGTH_SHORT).show();
                view.setBackgroundResource(R.drawable.button_shape);
            }
            @Override
            public void onOverflowMenuClick(View v, final int position) {
                PopupMenu popupMenu=new PopupMenu(getActivity(),v);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        HashMap task;
                        switch (item.getItemId())
                        {
                            case R.id.action_delete:
                                myTaskDB.removeTaskFromDatabase(taskFirebaseRecylerAdapter.getItem(position));
                                return true;

                            case R.id.action_duplicate:
                                Database.TaskData tasktoDuplicate=taskFirebaseRecylerAdapter.getItem(position);
                                tasktoDuplicate.setId(tasktoDuplicate.getId()+"_new");
                                myTaskDB.addTaskToDatabase(tasktoDuplicate);
                                return  true;

                            default:
                                return false;
                        }
                    }
                });
                MenuInflater inflater=popupMenu.getMenuInflater();
                inflater.inflate(R.menu.task_contextual_menu,popupMenu.getMenu());
                popupMenu.show();
            }
        });
        defaultanimation();
        adapteranimation();
        itemanimation();
        return view;
    }

//    void AddTaskFragmentToDB() {
//        AddTaskFragment addTask = AddTaskFragment.newInstance(userId);
//        addTask.setEventCallback(new AddTaskFragment.eventTaskCallback() {
//            @Override
//            public void onTaskSubmit(Database.TaskData myTask) {
//                myTaskDB.addTaskToDatabase(myTask);
//            }
//        });
//    }

    class ActionBarCallBack implements ActionMode.Callback {
        int position;

        public ActionBarCallBack(int position) {
            this.position = position;
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.task_contextual_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            int id = item.getItemId();
            switch (id) {
                case R.id.action_delete:


                    mode.finish();
                    break;
                case R.id.action_duplicate:

                    mode.finish();
                    break;
                default:
                    break;

            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {

        }
    }

    private  void  defaultanimation()
    {

        DefaultItemAnimator animator=new DefaultItemAnimator();
        animator.setAddDuration(3000);
        animator.setRemoveDuration(100);
        recyclerView.setItemAnimator(animator);
    }

    private void adapteranimation()
    {

        AlphaInAnimationAdapter alphaInAnimationAdapter=new AlphaInAnimationAdapter(taskFirebaseRecylerAdapter);
        ScaleInAnimationAdapter scaleInAnimationAdapter=new ScaleInAnimationAdapter(alphaInAnimationAdapter);
        recyclerView.setAdapter(scaleInAnimationAdapter);
    }

    private void itemanimation()
    {
        ScaleInAnimator animator=new ScaleInAnimator();
        animator.setInterpolator(new OvershootInterpolator());
        animator.setAddDuration(300);
        animator.setRemoveDuration(300);
        recyclerView.setItemAnimator(animator);
    }

    public void AddNewTask(){
       // Toast.makeText(getActivity(),"Add Task",Toast.LENGTH_LONG).show();
        getActivity().getSupportFragmentManager().beginTransaction().
                replace(R.id.container2, AddTaskFragment.newInstance(userId,myTaskDB)).addToBackStack(null)
                .commit();
    }

}
