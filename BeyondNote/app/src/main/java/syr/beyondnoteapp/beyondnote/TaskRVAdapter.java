package syr.beyondnoteapp.beyondnote;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;

import java.util.Random;


public class TaskRVAdapter extends FirebaseRecyclerAdapter<Database.TaskData, TaskRVAdapter.TaskViewHolder> {

    private Context mContext;
    int priorityImg[]=new int[]{R.drawable.silverstarimg,R.drawable.goldstartimg,R.drawable.redstarpr};
    static TaskRVAdapter.OnItemClickListener mItemClickListener;

    public TaskRVAdapter(Class<Database.TaskData> modelClass, int modelLayout, Class<TaskViewHolder> holder, DatabaseReference ref, Context context) {
        super(modelClass, modelLayout, holder, ref);
        this.mContext = context;
    }

    public void setOnItemClickListener ( final TaskRVAdapter.OnItemClickListener
                                                 mItemClickListener ) {
        this.mItemClickListener =  mItemClickListener;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
        public void onItemLongClick(View view, int position);
        public  void onOverflowMenuClick(View view, int position);
    }
    @Override
    protected void populateViewHolder(TaskViewHolder taskViewHolder, Database.TaskData task, int i) {

        //TODO: Populate viewHolder by setting the movie attributes to cardview fields


        taskViewHolder.titleName.setText(task.getTitle());
        taskViewHolder.taskDesc.setText(task.getDescription());
        taskViewHolder.date.setText(task.getDate());
        taskViewHolder.time.setText(task.getTime());
        int resID = 0;
        String priorityId=task.getPriority();
        if(!priorityId.equals("-1") && !priorityId.equals("") ){
//            TypedArray imgs = mContext.getResources().obtainTypedArray(R.array.priorityImgs);        //id from aarays.xml in values folder
//            while(Integer.parseInt(task.getPriority())!=resID){
//                final Random rand = new Random();
//                final int rndInt = rand.nextInt(imgs.length());
//                resID = imgs.getResourceId(rndInt, 0);
//                taskViewHolder.priorityImg.setImageResource(resID);
//        }
            taskViewHolder.priorityImg.setImageResource(priorityImg[Integer.parseInt(task.getPriority())]);
        }
    }

    //TODO: Populate ViewHolder and add listeners.
    public static class TaskViewHolder extends RecyclerView.ViewHolder {

        public TextView titleName;
        public TextView taskDesc;
        public TextView date;
        public TextView time;
        public ImageView priorityImg;
        public ImageView menupopup;

        public TaskViewHolder(View v) {

            super(v);

            titleName = (TextView)
                    v.findViewById(R.id.taskName);
            taskDesc = (TextView)
                    v.findViewById(R.id.taskdDesc);
            date=(TextView)
                    v.findViewById(R.id.task_date);
            time = (TextView)
                    v.findViewById(R.id.task_time);
            priorityImg = (ImageView)
                    v.findViewById(R.id.task_prorityImage);
            menupopup = (ImageView)
                    v.findViewById(R.id.menupopup);


            if(menupopup!=null)
            {
                menupopup.setOnClickListener(new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        if(mItemClickListener!=null)
                        {
                            mItemClickListener.onOverflowMenuClick(v,getPosition());
                        }
                    }
                });
            }

            v.setOnClickListener ( new View.OnClickListener () {
                @Override
                public void onClick ( View v ) {
                    if( mItemClickListener != null ) {
                        if( getAdapterPosition () !=
                                RecyclerView.NO_POSITION )
                        {
                            mItemClickListener.onItemClick(v,getAdapterPosition());
                        }
                    }
                }
            });

            v.setOnLongClickListener ( new View.OnLongClickListener()
            {
                @Override
                public boolean onLongClick ( View v) {
                    if( mItemClickListener != null ) {
                        if( getAdapterPosition () !=
                                RecyclerView.NO_POSITION ) {
                            mItemClickListener.onItemLongClick (v ,
                                    getAdapterPosition () );
                        }
                    }
                    return true ;
                }
            }) ;
        }
    }


}
