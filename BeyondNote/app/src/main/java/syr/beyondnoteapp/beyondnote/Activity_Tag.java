package syr.beyondnoteapp.beyondnote;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.List;
import java.util.Map;

public class Activity_Tag extends AppCompatActivity {

    String userEmailId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag);

        Intent intentExtras = getIntent();
        userEmailId = intentExtras.getStringExtra("email");

        Log.d("Email : ", userEmailId);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.tag_frag_container, TagsViewPagerFragment.newInstance(userEmailId))
                .commit();
    }
}
