package syr.beyondnoteapp.beyondnote;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.firebase.ui.database.ChangeEventListener;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;

import java.util.List;
import java.util.Map;

public class NotesRecyclerViewFirebaseAdapter extends FirebaseRecyclerAdapter<Database.NotesData, NotesRecyclerViewFirebaseAdapter.NotesViewHolder> {

    OnNotesItemClickListener mNotesItemClickListner;

    public NotesRecyclerViewFirebaseAdapter(Class<Database.NotesData> NotesClass,
                                            int CardViewLayout,
                                            Class<NotesViewHolder> holder,
                                            DatabaseReference DbRef)
    {
        super(NotesClass, CardViewLayout, holder, DbRef);
    }

    public class NotesViewHolder extends RecyclerView.ViewHolder {

        public TextView Notes_cv_title;
        public TextView Notes_cv_desc;
        //public ImageView Notes_cv_image;
        final ImageView Notes_cv_hamburger;
        public CardView MyCardView;

        public NotesViewHolder(View v) {
            //Initialize base class (RecyclerView.ViewHolder) members
            super(v);


            Notes_cv_title = (TextView) v.findViewById(R.id.notes_cv_title);
            Notes_cv_desc = (TextView) v.findViewById(R.id.notes_cv_description);
            //Notes_cv_image = (ImageView) v.findViewById(R.id.notes_cv_imgview);
            Notes_cv_hamburger = (ImageView) v.findViewById(R.id.notes_cv_hamburger);
            MyCardView = (CardView) v.findViewById(R.id.notes_card_view);

            Notes_cv_hamburger.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mNotesItemClickListner != null) {
                        mNotesItemClickListner.onNoteHamburgerIconClick(v, getAdapterPosition());
                    }
                }
            });
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mNotesItemClickListner != null) {
                        mNotesItemClickListner.onNoteItemClick(v, getAdapterPosition());
                    }
                }
            });
            v.setOnLongClickListener (new View.OnLongClickListener () {
                @Override
                public boolean onLongClick(View v) {
                    if (mNotesItemClickListner != null) {
                        mNotesItemClickListner.onNoteItemLongClick(v, getAdapterPosition());
                    }
                    return true;
                }
            });
        }

        public void bindNotesData(Database.NotesData myNote) {
            Notes_cv_title.setText(myNote.title);
            Notes_cv_desc.setText(myNote.content);
            MyCardView.setBackgroundColor(Integer.parseInt(myNote.color));

            //todo - put image
            //Picasso.with(mContext).load(movie.getUrl()).into(movieViewHolder.MovieImage);
            //Notes_cv_image.setImageResource(R.color.cardview_light_background);
        }
    }

    //Define interface used by app to set event listener
    public interface OnNotesItemClickListener {
        public void onNoteItemClick(View view, int position);
        public void onNoteItemLongClick(View view, int position);
        public void onNoteHamburgerIconClick(View view, int position);
    }

    //Allow app to set event listener
    public void setOnNotesItemClickListener(final OnNotesItemClickListener mNotesItemClickListner) {
        this.mNotesItemClickListner = mNotesItemClickListner;
    }

    @Override
    public NotesViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v;

        v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.notes_card_view, viewGroup, false);

        NotesViewHolder pvh = new NotesViewHolder(v);

        return pvh;
    }

    @Override
    protected void populateViewHolder(NotesViewHolder notesViewHolder, Database.NotesData MyNote, int position) {
        notesViewHolder.bindNotesData(MyNote);
    }
}
