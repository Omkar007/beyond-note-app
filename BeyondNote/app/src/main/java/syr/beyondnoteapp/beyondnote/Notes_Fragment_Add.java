package syr.beyondnoteapp.beyondnote;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManagerNonConfig;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.facebook.FacebookSdk.getApplicationContext;

public class Notes_Fragment_Add extends Fragment {

    private static final String ARG_Note_Instance = "NoteDataInstance";
    public static final int RESULT_OK = -1;

    boolean isNewNote;
    Database.NotesData MyNote;
    ActionBar actionBar;
    Toolbar toolbarColor;
    RelativeLayout MyLayout;
    LinearLayout MyLayoutH;
    FrameLayout MyLayoutF;
    CircleImageView CurrentView;
    CharSequence ActionBarTitle;

    final int MAX_IMAGE_COUNT = 3;
    int imageCount = 0;
    LinearLayout picsLL;
    int picsLayoutWidth;
    int picsLayoutHeight;

    List<ImageView> MyImgViewList;
    List<String> ImageUploadPath;
    List<String> LocalImagePath;

    //Added for Image Browse and Upload
    private static final int PICK_IMAGE_REQUEST = 234;
    private static final int CAMERA_REQUEST = 1888;
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private String mCurrentPhotoPath;

    //firebase storage/database reference
    private StorageReference storageReference;

    /* Add interface to allow calling activity/fragment to communicate */
    public interface doEventCallback {
        //Pass the generated note to calling activity/fragment
        //It would want to save it in database or display to user
        void onNoteCreate(Database.NotesData MyNoteData);

        void onNoteUpdate(Database.NotesData MyNoteData);

        void configureActionBar();
    }

    doEventCallback eventCallback;

    /**
     * Use this factory method to create a new instance of
     * this fragment
     *
     * @return A new instance of fragment Fragment_Notes.
     */
    public static Notes_Fragment_Add newInstance(Database.NotesData MyNote) {
        Notes_Fragment_Add fragment = new Notes_Fragment_Add();

        Bundle args = new Bundle();
        args.putSerializable(ARG_Note_Instance, MyNote);
        fragment.setArguments(args);

        return fragment;
    }

    public Notes_Fragment_Add() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            MyNote = (Database.NotesData) getArguments().getSerializable(ARG_Note_Instance);
        }
    }

    /* allow calling activity/fragment to implement interface */
    public void setEventCallback(doEventCallback customCallback) {
        eventCallback = customCallback;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.notes_fragment_add, container, false);

        //Set this as fragment was to add more menu items to activity actionbar
        setHasOptionsMenu(true);

        //Set title and save calling screen so that we can reset on destroy
        actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        ActionBarTitle = actionBar.getTitle();

        //counter check if activity is destroyed
        if (eventCallback != null) {
            eventCallback.configureActionBar();
        }

        //Get layout to change background color
        MyLayout = (RelativeLayout) rootView.findViewById(R.id.note_add_layout);
        MyLayoutH = (LinearLayout) rootView.findViewById(R.id.note_add_header_layout);
        MyLayoutF = (FrameLayout) rootView.findViewById(R.id.note_add_footer_layout);


        //Set Handlers for color picker toolbar
        toolbarColor = (Toolbar) rootView.findViewById(R.id.note_add_toolbar_color);
        setupToolbarColorItemSelected(rootView);

        //Set scrolling property for Title and Content text view
        //todo resolve onClick bug - two clicks are required to clear content
        final EditText tvTitle = (EditText) rootView.findViewById(R.id.note_add_title);
        tvTitle.setMovementMethod(new ScrollingMovementMethod());
        //tvTitle.setOnClickListener(AddNoteClickHandler);
//        tvTitle.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                if ((tvTitle.getText().toString().equals(getString(R.string.note_default_title))))
//                    tvTitle.setText("");
//                return true;
//            }
//        });

        final EditText tvContent = (EditText) rootView.findViewById(R.id.note_add_content);
        tvContent.setMovementMethod(new ScrollingMovementMethod());
        //tvContent.setOnClickListener(AddNoteClickHandler);
//        tvContent.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                if ((tvContent.getText().toString().equals(getString(R.string.note_default_content))))
//                    tvContent.setText("");
//                return true;
//            }
//        });

        //getting firebase storage reference
        storageReference = FirebaseStorage.getInstance().getReference();
        ImageUploadPath = new ArrayList<>();
        LocalImagePath = new ArrayList<>();

        //manage adding pics
        picsLL = (LinearLayout) rootView.findViewById(R.id.add_note_layout_pics);
        picsLL.post(new Runnable() {
            @Override
            public void run() {
                picsLayoutWidth = picsLL.getWidth();
                picsLayoutHeight = picsLL.getHeight();

                //todo Add existing images here
                //addImage(R.drawable.navigation_header_image2);
            }
        });

        if (MyNote.id == null || MyNote.id.equals("")) {
            NewNoteSetup(rootView);
        } else {
            PopulateMyNote(rootView);
        }

        return rootView;
    }

    //void addImage(final int imageResource)
    void addImage(final Bitmap imageResource) {
        if (imageCount >= MAX_IMAGE_COUNT) {
            Toast.makeText(getActivity(), "Can't add more Images", Toast.LENGTH_SHORT).show();
            return;
        }

        if (MyImgViewList == null) {
            MyImgViewList = new ArrayList<>();
        }

        imageCount++;

        final LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(picsLayoutWidth / imageCount, picsLayoutHeight);
        final ImageView myImage = new ImageView(getContext());
        //myImage.setImageResource(imageResource);
        myImage.setImageBitmap(imageResource);
        myImage.setLayoutParams(parms);
        myImage.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        MyImgViewList.add(myImage);

        //To avoid race condition use runnable
        picsLL.post(new Runnable() {
            @Override
            public void run() {
                //resize all existing images on UI
                for (ImageView images : MyImgViewList) {
                    images.setLayoutParams(parms);
                    //images.invalidate();
                }
                //todo this can be improved for better viewing
                picsLL.addView(myImage);
                //picsLL.invalidate();
            }
        });
    }

    void addImageDownload(final String ImageName) {
        if (imageCount >= MAX_IMAGE_COUNT) {
            Toast.makeText(getActivity(), "Can't add more Images", Toast.LENGTH_SHORT).show();
            return;
        }

        if (MyImgViewList == null) {
            MyImgViewList = new ArrayList<>();
        }

        imageCount++;

        final LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(picsLayoutWidth / imageCount, picsLayoutHeight);
        final ImageView myImage = new ImageView(getContext());
        //myImage.setImageResource(imageResource);
        //myImage.setImageBitmap(imageResource);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                downloadImageFile(ImageName, myImage);
            }
        }, 100);

        myImage.setLayoutParams(parms);
        MyImgViewList.add(myImage);

        //To avoid race condition use runnable
        picsLL.post(new Runnable() {
            @Override
            public void run() {
                //resize all existing images on UI
                for (ImageView images : MyImgViewList) {
                    images.setLayoutParams(parms);
                    images.invalidate();
                }
                //todo this can be improved for better viewing
                picsLL.addView(myImage);
                picsLL.invalidate();
            }
        });
    }

    void NewNoteSetup(View v) {
        //actionBar.setTitle("New Note");

        //Set Current Date Time
        TextView tvDate = (TextView) v.findViewById(R.id.note_add_date);
        tvDate.setText(Utility.GetData());

        TextView tvClock = (TextView) v.findViewById(R.id.note_add_time);
        tvClock.setText(Utility.GetTime());

        isNewNote = true;
    }

    void PopulateMyNote(View v) {
        ((EditText) v.findViewById(R.id.note_add_title)).setText(MyNote.getTitle());
        ((EditText) v.findViewById(R.id.note_add_content)).setText(MyNote.getContent());
        ((TextView) v.findViewById(R.id.note_add_date)).setText(MyNote.getDate());
        ((TextView) v.findViewById(R.id.note_add_time)).setText(MyNote.getTime());

        int noteColor = Integer.parseInt(MyNote.getColor());
        MyLayout.setBackgroundColor(noteColor);
        MyLayoutH.setBackgroundColor(noteColor);
        MyLayoutF.setBackgroundColor(noteColor);

        //color
        CurrentView.setImageResource(android.R.color.transparent);
        if (noteColor == getColor(R.color.notes_color_1)) {
            CurrentView = (CircleImageView) v.findViewById(R.id.note_add_toolbar_item_color1);
        } else if (noteColor == getColor(R.color.notes_color_2)) {
            CurrentView = (CircleImageView) v.findViewById(R.id.note_add_toolbar_item_color2);
        } else if (noteColor == getColor(R.color.notes_color_3)) {
            CurrentView = (CircleImageView) v.findViewById(R.id.note_add_toolbar_item_color3);
        } else if (noteColor == getColor(R.color.notes_color_4)) {
            CurrentView = (CircleImageView) v.findViewById(R.id.note_add_toolbar_item_color4);
        } else if (noteColor == getColor(R.color.notes_color_5)) {
            CurrentView = (CircleImageView) v.findViewById(R.id.note_add_toolbar_item_color5);
        } else if (noteColor == getColor(R.color.notes_color_6)) {
            CurrentView = (CircleImageView) v.findViewById(R.id.note_add_toolbar_item_color6);
        } else {
            Log.e("ColorNotFound", MyNote.getColor());
        }
        CurrentView.setImageResource(R.drawable.icon_check);

        Log.d("PopulateMyNote All", "Images: " + MyNote.getImages());
        if(MyNote.getImages().equals("")) {
            Log.d("PopulateMyNote", "Image String empty");
        }
        //todo populate images
        if(!MyNote.getImages().equals("")) {
            try {

                String[] MyImageList = MyNote.getImages().split(";");
                for (String imageStr : MyImageList) {
                    Log.d("PopulateMyNote Image", imageStr);
                    Log.d("PopulateMyNote Uri", "" + Uri.parse(imageStr));

                    //addImageDownload(imageStr);

                    //getActivity().getContentResolver().takePersistableUriPermission(Uri.parse(imageStr), Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), Uri.parse(imageStr));
                    //Bitmap bitmap = uriToBitmap(Uri.parse(imageStr));
//                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT, Uri.parse(imageStr));
//                intent.addCategory(Intent.CATEGORY_OPENABLE);
//                intent.setType("image/*");
//                startActivityForResult(intent, 999);
                    if (bitmap != null)
                        addImage(bitmap);
                    else
                        Log.d("Populate Img", "bitmap is null");
                    LocalImagePath.add(imageStr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        isNewNote = false;
    }

    private Bitmap uriToBitmap(Uri selectedFileUri) {
        Bitmap image = null;
        try {
            ParcelFileDescriptor parcelFileDescriptor =
                    getActivity().getContentResolver().openFileDescriptor(selectedFileUri, "r");
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
            parcelFileDescriptor.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }

    int getColor(int ColorResource) {
        int Color;
        try {

            //Get Color String in Hex. Note below is an annotation to suppress string error.
            //See http://tools.android.com/tips/lint/suppressing-lint-warnings
            //noinspection ResourceType
            //String ColorStr = getResources().getString(ColorResource);

            Color = ContextCompat.getColor(getActivity(), ColorResource);
        } catch (NullPointerException e) {
            Color = -1; //White
        }
        return Color;
    }


    //Add Handler
//    View.OnClickListener AddNoteClickHandler = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            int id = v.getId();
//
//            switch (id) {
//                case R.id.note_add_title:
//                    if (((TextView) v).getText().toString().equals(getString(R.string.note_default_title)))
//                        ((TextView) v).setText("");
//                    break;
//                case R.id.note_add_content:
//                    if (((TextView) v).getText().toString().equals(getString(R.string.note_default_content)))
//                        ((TextView) v).setText("");
//                    break;
//                default:
//                    break;
//            }
//        }
//    };

    View.OnClickListener AddColorClickHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CircleImageView MyColor = (CircleImageView) v;

            MyLayout.setBackgroundColor(MyColor.getFillColor());
            MyLayoutH.setBackgroundColor(MyColor.getFillColor());
            MyLayoutF.setBackgroundColor(MyColor.getFillColor());
            MyColor.setImageResource(R.drawable.icon_check);

            CurrentView.setImageResource(android.R.color.transparent);
            CurrentView = MyColor;
        }
    };

    private void setupToolbarColorItemSelected(View rootView) {
//        toolbarColor.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener(){
//
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                int id = item.getItemId();
//
//                switch (id) {
//                    case R.id.note_add_toolbar_item_color1:
//                        Toast.makeText(getActivity(), "Color 1", Toast.LENGTH_SHORT).show();
//                        return true;
//                    default:
//                        Toast.makeText(getActivity(), "ID" + id, Toast.LENGTH_SHORT).show();
//                        break;
//                }
//                return false;
//            }
//        });

        CircleImageView MyCircleImageView;
        MyCircleImageView = (CircleImageView) rootView.findViewById(R.id.note_add_toolbar_item_color1);
        MyCircleImageView.setOnClickListener(AddColorClickHandler);
        CurrentView = MyCircleImageView;

        MyCircleImageView = (CircleImageView) rootView.findViewById(R.id.note_add_toolbar_item_color2);
        MyCircleImageView.setOnClickListener(AddColorClickHandler);
        MyCircleImageView = (CircleImageView) rootView.findViewById(R.id.note_add_toolbar_item_color3);
        MyCircleImageView.setOnClickListener(AddColorClickHandler);
        MyCircleImageView = (CircleImageView) rootView.findViewById(R.id.note_add_toolbar_item_color4);
        MyCircleImageView.setOnClickListener(AddColorClickHandler);
        MyCircleImageView = (CircleImageView) rootView.findViewById(R.id.note_add_toolbar_item_color5);
        MyCircleImageView.setOnClickListener(AddColorClickHandler);
        MyCircleImageView = (CircleImageView) rootView.findViewById(R.id.note_add_toolbar_item_color6);
        MyCircleImageView.setOnClickListener(AddColorClickHandler);
    }

    @Override
    public void onDestroyView() {

        if (isNewNote) {
            //If note is edited, save in database
            if (isNoteEdited()) {

                if (eventCallback != null) {
                    //Pass generated note to calling activity
                    eventCallback.onNoteCreate(getNotesData());
                    Toast.makeText(getActivity(), "Saved Note", Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            //If note is edited, save in database
            if (isNoteUpdated()) {

                if (eventCallback != null) {
                    //Pass generated note to calling activity
                    Database.NotesData note = getNotesData();
                    note.setId(MyNote.id);
                    eventCallback.onNoteUpdate(note);
                    Toast.makeText(getActivity(), "Updated Note", Toast.LENGTH_SHORT).show();
                }
            }

        }

        //Reset title to title from calling screen
        actionBar.setTitle(ActionBarTitle);

        super.onDestroyView();
    }

    //Check if content is added to Note
    private boolean isNoteEdited() {
        EditText tvTitle = (EditText) getView().findViewById(R.id.note_add_title);
        Editable text = tvTitle.getText();
        if (text.toString() != "")
            return true;

        EditText tvContent = (EditText) getView().findViewById(R.id.note_add_content);
        Editable ContentText = tvContent.getText();
        if (ContentText.toString() != "")
            return true;

        //check if images are update
        if(!GetImageStringFromList(LocalImagePath).equals(""))
            return true;

//        if(!GetImageStringFromList(ImageUploadPath).equals(""))
//            return true;

        return false;
    }

    private boolean isNoteUpdated() {
        EditText tvTitle = (EditText) getView().findViewById(R.id.note_add_title);
        Editable text = tvTitle.getText();
        if (!text.toString().equals(MyNote.getTitle()))
            return true;

        EditText tvContent = (EditText) getView().findViewById(R.id.note_add_content);
        Editable ContentText = tvContent.getText();
        if (!ContentText.toString().equals(MyNote.getContent()))
            return true;

        if (!Integer.toString(CurrentView.getFillColor()).equals(MyNote.getColor()))
            return true;

        //check if images are update
        if(!MyNote.getImages().equals(GetImageStringFromList(LocalImagePath)))
            return true;

        return false;
    }

    private Database.NotesData getNotesData() {
        Database.NotesData MyNote = new Database.NotesData();

        MyNote.title = ((EditText) getView().findViewById(R.id.note_add_title)).getText().toString();
        MyNote.content = ((EditText) getView().findViewById(R.id.note_add_content)).getText().toString();
        MyNote.color = Integer.toString(CurrentView.getFillColor());
        MyNote.date = ((TextView) getView().findViewById(R.id.note_add_date)).getText().toString();
        MyNote.time = ((TextView) getView().findViewById(R.id.note_add_time)).getText().toString();
        MyNote.tags = getTags();

        MyNote.images = GetImageStringFromList(LocalImagePath);
        //MyNote.images = GetImageStringFromList(ImageUploadPath);
        Log.d("getNotesData Images" , MyNote.images);

        return MyNote;
    }

    private String GetImageStringFromList(List<String> MyStrList) {
        String str = "";
        for(String images: MyStrList) {
            str += images + ";";
        }
        return str;
    }

    private String getTags() {
        String content = ((TextView) getView().findViewById(R.id.note_add_content)).getText().toString();
        String tags = "";

        int start;
        for (int i = 0; i < content.length(); i++)
            if (content.charAt(i) == '#') {
                i++;    //Skip #
                start = i;
                while (i < content.length() && Character.isLetterOrDigit(content.charAt(i)))
                    i++;    //continue till end of word
                if (start != i)
                    tags += content.substring(start, i) + ",";
            }

        //remove "," at end of string
        if (tags.length() > 0)
            return tags.substring(0, tags.length() - 1);
        else
            return "";
    }

    //For overflow menu - inflater
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (menu.findItem(R.id.notes_add_gallery) == null)
            inflater.inflate(R.menu.notes_add_menu, menu);
    }

    //For overflow menu - event handler
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.notes_add_camera:
                if (imageCount >= MAX_IMAGE_COUNT)
                    Toast.makeText(getActivity(), "Can't add more Images", Toast.LENGTH_SHORT).show();
                else
                    showCamera();
                return true;
            case R.id.notes_add_gallery:
                if (imageCount >= MAX_IMAGE_COUNT)
                    Toast.makeText(getActivity(), "Can't add more Images", Toast.LENGTH_SHORT).show();
                else
                    showFileChooser();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showCamera()
    {
        PackageManager packageManager = getActivity().getPackageManager();
        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Toast.makeText(getActivity(), "Cannot open system camera", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Create the File where the photo should go
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException ex) {
            // Error occurred while creating the File
            Log.i("showCamera", "IOException" + ex);
        }
//
//        if (checkSelfPermission(Manifest.permission.CAMERA)
//                != PackageManager.PERMISSION_GRANTED) {
//
//            requestPermissions(new String[]{Manifest.permission.CAMERA},
//                    MY_REQUEST_CODE);
//        }

        // Continue only if the File was successfully created
        if (photoFile != null) {
            Uri photoURI = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", photoFile);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        Log.i("showCamera", storageDir.getPath());
        File image = new File (
                storageDir,
                "JPEG_" + timeStamp + ".jpg"
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("ActResCalled", "" + requestCode);

        // RC_SIGN_IN is the request code you passed into startActivityForResult(...) when starting the sign in flow.
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                Log.d("Img filepath:", "" + filePath);
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                addImage(bitmap);
                LocalImagePath.add(filePath.toString());
                //uploadImage(filePath);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
//            Bitmap photo = (Bitmap) data.getExtras().get("data");
//            addImage(photo);
//            //LocalImagePath.add(filePath.toString());
//        }
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), Uri.parse(mCurrentPhotoPath));
                addImage(bitmap);
                //uploadImage(Uri.parse(mCurrentPhotoPath));
                LocalImagePath.add(mCurrentPhotoPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (requestCode == 999 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                Log.d("Img filepath:", "" + filePath);
                getActivity().getContentResolver().takePersistableUriPermission(filePath, Intent.FLAG_GRANT_READ_URI_PERMISSION
                        | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                addImage(bitmap);
                LocalImagePath.add(filePath.toString());
                //uploadImage(filePath);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void uploadImage(Uri filePath)
    {
        if (filePath == null) {
            return;
        }

        StorageReference riversRef = storageReference.child("images/").child(filePath.getLastPathSegment());

        riversRef.putFile(filePath)
            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                @SuppressWarnings("VisibleForTests")
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    String ImagePath = String.valueOf(taskSnapshot.getDownloadUrl());
                    Log.d("uploadImage Path: ", ImagePath);
                    ImageUploadPath.add(ImagePath);
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    //if the upload is not successfull
                    Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_LONG).show();
                }
            })
            .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                @SuppressWarnings("VisibleForTests")
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                }
            });
    }

    public void downloadImageFile(String ImageName, final ImageView iv){
        StorageReference riversRef = storageReference.child("images/").child(ImageName+".jpg");
        riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {

                Log.d("image path",uri.toString());
                Picasso.with(getActivity()).load(uri).noPlaceholder().centerCrop().fit().into(iv);
                iv.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                iv.invalidate();
                /// The string(file link) that you need
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                Toast.makeText(getApplicationContext(),"failed to retrieve Image",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
