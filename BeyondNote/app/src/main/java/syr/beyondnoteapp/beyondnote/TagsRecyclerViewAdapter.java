package syr.beyondnoteapp.beyondnote;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

public class TagsRecyclerViewAdapter extends RecyclerView.Adapter<TagsRecyclerViewAdapter.TagsViewHolder> {

    private Context mContext;
    private List<Map<String, String>> mnotes_task_List;
    OnTagsItemClickListener mTagsItemClickListner;

    TagsRecyclerViewAdapter(Context context, List<Map<String, String>> notes_task_List) {
        this.mContext = context;
        mnotes_task_List = notes_task_List;
    }

    public class TagsViewHolder extends RecyclerView.ViewHolder {
        public TextView tags_cv_title;
        public TextView tags_cv_desc;
        public TextView tags_cv_tags;
        public ImageView tags_cv_image;
        public CardView tags_cv;

        public TagsViewHolder(View v) {
            //Initialize base class (RecyclerView.ViewHolder) members
            super(v);

            tags_cv_title = (TextView) v.findViewById(R.id.tags_cv_title);
            tags_cv_desc = (TextView) v.findViewById(R.id.tags_cv_description);
            tags_cv_tags = (TextView) v.findViewById(R.id.tags_cv_tags);
            tags_cv_image = (ImageView) v.findViewById(R.id.tags_cv_icon);
            tags_cv = (CardView) v.findViewById(R.id.tags_card_view);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mTagsItemClickListner != null) {
                        mTagsItemClickListner.onTagItemClick(v, getAdapterPosition());
                    }
                }
            });
            v.setOnLongClickListener (new View.OnLongClickListener () {
                @Override
                public boolean onLongClick(View v) {
                    if (mTagsItemClickListner != null) {
                        mTagsItemClickListner.onTagItemLongClick(v, getAdapterPosition());
                    }
                    return true;
                }
            });
        }

        public void bindNotesData(Map<String, String> myNoteTask) {

            if((String)myNoteTask.get("priority") == null) { //NOtes
                tags_cv_title.setText((String)myNoteTask.get("title"));
                tags_cv_desc.setText((String)myNoteTask.get("content"));
                tags_cv_tags.setText((String)myNoteTask.get("tags"));

                tags_cv.setBackgroundColor(Integer.parseInt(myNoteTask.get("color")));
                tags_cv_image.setImageResource(R.drawable.icon_notes);
            } else { //Task
                tags_cv_title.setText((String)myNoteTask.get("title"));
                tags_cv_desc.setText((String)myNoteTask.get("description"));
                tags_cv_tags.setText((String)myNoteTask.get("tags"));

                tags_cv_image.setImageResource(R.drawable.icon_tasks);

            }
        }
    }

    //Define interface used by app to set event listener
    public interface OnTagsItemClickListener {
        public void onTagItemClick(View view, int position);
        public void onTagItemLongClick(View view, int position);
    }

    //Allow app to set event listener
    public void setOnNotesItemClickListener(final OnTagsItemClickListener mTagsItemClickListner) {
        this.mTagsItemClickListner = mTagsItemClickListner;
    }

    @Override
    public TagsRecyclerViewAdapter.TagsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v;

        v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.tags_card_view, viewGroup, false);

        TagsViewHolder pvh = new TagsViewHolder(v);

        return pvh;
    }

    @Override
    public void onBindViewHolder(TagsViewHolder MyViewHolder, int i) {
        Map<String, String> myNoteTask = mnotes_task_List.get(i);
        MyViewHolder.bindNotesData(myNoteTask);
    }

    @Override
    public int getItemCount() {
        return mnotes_task_List.size();
    }
}
