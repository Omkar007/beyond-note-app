package syr.beyondnoteapp.beyondnote;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Omkar on 4/24/2017.
 *
 * OAuthToken class is used to store the bearer token we request from Twitter with our consumer key and secret.
 *  @SerializedName annotation to set the name Retrofit (de)serializes the fields with
 */

public class OAuthToken {

    @SerializedName("access_token")
    private String accessToken;

    @SerializedName("token_type")
    private String tokenType;

    public String getAccessToken() {
        return accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public String getAuthorization() {
        return getTokenType() + " " + getAccessToken();
    }
}
