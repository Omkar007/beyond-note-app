package syr.beyondnoteapp.beyondnote;

import android.support.v7.widget.RecyclerView;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

public class NotesRecyclerViewAdapter extends RecyclerView.Adapter<NotesRecyclerViewAdapter.NotesViewHolder> {

    private Context mContext;
    private List<Map<String, String>> myNotes;
    OnNotesItemClickListener mNotesItemClickListner;

    NotesRecyclerViewAdapter (Context context, List<Map<String, String>> notesList) {
        this.mContext = context;
        myNotes = notesList;
    }

    public class NotesViewHolder extends RecyclerView.ViewHolder {

        public TextView Notes_cv_title;
        public TextView Notes_cv_desc;
        //public ImageView Notes_cv_image;

        public NotesViewHolder(View v) {
            //Initialize base class (RecyclerView.ViewHolder) members
            super(v);

            Notes_cv_title = (TextView) v.findViewById(R.id.notes_cv_title);
            Notes_cv_desc = (TextView) v.findViewById(R.id.notes_cv_description);
            //Notes_cv_image = (ImageView) v.findViewById(R.id.notes_cv_imgview);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mNotesItemClickListner != null) {
                        mNotesItemClickListner.onNoteItemClick(v, getAdapterPosition());
                    }
                }
            });
            v.setOnLongClickListener (new View.OnLongClickListener () {
                @Override
                public boolean onLongClick(View v) {
                    if (mNotesItemClickListner != null) {
                        mNotesItemClickListner.onNoteItemLongClick(v, getAdapterPosition());
                    }
                    return true;
                }
            });
        }

        public void bindNotesData(Map<String, ?> myNote) {
            Notes_cv_title.setText((String)myNote.get("title"));
            Notes_cv_desc.setText((String)myNote.get("content"));
            //Notes_cv_image.setImageResource(R.drawable.icon_user_default);
        }
    }

    //Define interface used by app to set event listener
    public interface OnNotesItemClickListener {
        public void onNoteItemClick(View view, int position);
        public void onNoteItemLongClick(View view, int position);
    }

    //Allow app to set event listener
    public void setOnNotesItemClickListener(final OnNotesItemClickListener mNotesItemClickListner) {
        this.mNotesItemClickListner = mNotesItemClickListner;
    }

    @Override
    public NotesRecyclerViewAdapter.NotesViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v;

        v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.notes_card_view, viewGroup, false);

        NotesViewHolder pvh = new NotesViewHolder(v);

        return pvh;
    }

    @Override
    public void onBindViewHolder(NotesViewHolder MyViewHolder, int i) {
        Map<String, ?> myNote = myNotes.get(i);
        MyViewHolder.bindNotesData(myNote);
    }

    @Override
    public int getItemCount() {
        return myNotes.size();
    }
}
