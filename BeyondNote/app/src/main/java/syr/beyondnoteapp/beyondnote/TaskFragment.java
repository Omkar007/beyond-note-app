package syr.beyondnoteapp.beyondnote;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;

public class TaskFragment extends Fragment {

    int size=0;
    static Database.TaskData hasTasks;

    private static  final String ARG_Section_number ="section_number";

    public static TaskFragment newInstance(Database.TaskData taskhashMap)
    {
        hasTasks=taskhashMap;
        TaskFragment fragment=new TaskFragment();
        Bundle args=new Bundle();
        args.putSerializable(ARG_Section_number,taskhashMap);
        fragment.setArguments(args);
        return fragment;
    }
    public TaskFragment()
    {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = null;
        view = inflater.inflate(R.layout.activity_task_fragment, container,
                false);

        if (savedInstanceState != null) {
            hasTasks= (Database.TaskData) savedInstanceState.get(ARG_Section_number);
        }

        TextView title=(TextView)view.findViewById(R.id.dtl_title);
        TextView description=(TextView)view.findViewById(R.id.dtl_decription);
        TextView date=(TextView)view.findViewById(R.id.dtl_date);
        TextView time=(TextView)view.findViewById(R.id.dtl_time);
        TextView contacts=(TextView)view.findViewById(R.id.dtl_contact);
        TextView location=(TextView)view.findViewById(R.id.dtl_task_location);
        TextView tags=(TextView)view.findViewById(R.id.dtl_hashTag);

        title.setText((String)hasTasks.getTitle());
        description.setText((String)hasTasks.getDescription());
        date.setText((String)hasTasks.getDate());
        time.setText((String)hasTasks.getTime());
        contacts.setText((String)hasTasks.getContacts());
        location.setText((String)hasTasks.getLocation());
        tags.setText((String)hasTasks.getTags());

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(ARG_Section_number,hasTasks);
    }
}
