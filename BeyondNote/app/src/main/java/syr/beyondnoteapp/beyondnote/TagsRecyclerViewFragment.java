package syr.beyondnoteapp.beyondnote;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

public class TagsRecyclerViewFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private Integer mIndex;
    RecyclerView rv;
    TagsRecyclerViewAdapter tagsRvAdapter;
    List<Map<String, String>> mnotes_task_List;

    //private OnFragmentInteractionListener mListener;

    public TagsRecyclerViewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TagsRecyclerViewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TagsRecyclerViewFragment newInstance(List<Map<String, String>> NoteTaskList) {
        TagsRecyclerViewFragment fragment = new TagsRecyclerViewFragment();

        //ToDO this will cause NULL value when screen orientation changes
        fragment.mnotes_task_List = NoteTaskList;
//        Bundle args = new Bundle();
//        args.putInt(ARG_PARAM1, Index);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mIndex = getArguments().getInt(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_tags_recycler_view, container, false);

        setHasOptionsMenu(true);

        //Initialize Recycler View
        rv = (RecyclerView) rootView.findViewById(R.id.recyclerViewTags);
        rv.setHasFixedSize(true);

        //Set Layout
        GridLayoutManager glm = new GridLayoutManager(getActivity(), 2);
        rv.setLayoutManager(glm);

        //Set Adapter
        InitializeTagsRvAdapter();

        return rootView;
    }

    void InitializeTagsRvAdapter()
    {
        tagsRvAdapter = new TagsRecyclerViewAdapter(getActivity(), mnotes_task_List);

        rv.setAdapter(tagsRvAdapter);

        tagsRvAdapter.setOnNotesItemClickListener(new TagsRecyclerViewAdapter.OnTagsItemClickListener() {

            @Override
            public void onTagItemClick(View v, int position) {
                Map<String, String> myNoteTask = mnotes_task_List.get(position);
                if((String)myNoteTask.get("priority") == null) { //NOtes
                    loadNoteFragment(myNoteTask);
                } else { //Task
                    loadTaskFragment(myNoteTask);
                }
            }

            @Override
            public void onTagItemLongClick(View v, int position) {
                Toast.makeText(getActivity(), "Long Click Position : " + position, Toast.LENGTH_SHORT).show();
            }
        });
    }

    void loadNoteFragment(Map<String, String> myNote)
    {
        Database.NotesData MyNote = new Database.NotesData();
        MyNote.setId(myNote.get("id"));
        MyNote.setTitle(myNote.get("title"));
        MyNote.setContent(myNote.get("content"));
        MyNote.setDate(myNote.get("date"));
        MyNote.setTime(myNote.get("time"));
        MyNote.setColor(myNote.get("color"));
        MyNote.setTags(myNote.get("tags"));
        MyNote.setImages(myNote.get("images"));

        Notes_Fragment_Add addNoteFragment = Notes_Fragment_Add.newInstance(MyNote);
        addNoteFragment.setEventCallback(new Notes_Fragment_Add.doEventCallback() {
            @Override
            public void onNoteCreate(Database.NotesData MyNoteData) {}

            @Override
            public void onNoteUpdate(Database.NotesData MyNoteData) {}

            @Override
            public void configureActionBar() {}
        });

        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.tag_frag_container, addNoteFragment)
                .addToBackStack(null)   //Ensure screen comes up when back is clicked from new note
                .commit();
    }

    void loadTaskFragment(Map<String, String> myTask)
    {
        Database.TaskData MyTask = new Database.TaskData();
        MyTask.setId(myTask.get("id"));
        MyTask.setTitle(myTask.get("title"));
        MyTask.setDescription(myTask.get("description"));
        MyTask.setPriority(myTask.get("priority"));
        MyTask.setDate(myTask.get("date"));
        MyTask.setTime(myTask.get("time"));
        MyTask.setTags(myTask.get("tags"));
        MyTask.setLocation(myTask.get("location"));
        MyTask.setContacts(myTask.get("contacts"));

        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.tag_frag_container, TaskFragment.newInstance(MyTask))
                .addToBackStack(null)   //Ensure screen comes up when back is clicked from new note
                .commit();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(menu.findItem(R.id.search) != null) {
            super.onCreateOptionsMenu(menu, inflater);
            return;
        }

        inflater.inflate(R.menu.search_menu, menu);
        MenuItem searchViewItem = menu.findItem(R.id.search);

        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);

        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchViewAndroidActionBar.clearFocus();
                //Toast.makeText(activity_recycler_view.this, "Position : " + index, Toast.LENGTH_SHORT).show();
                int index = getIndexFromName(query);
                rv.getLayoutManager().scrollToPosition(index);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Toast.makeText(activity_recycler_view.this, "Change", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    public int getIndexFromName(String Name)
    {
        int size = mnotes_task_List.size();
        for (int i=0; i<size; i++) {
            Map<String, ?> myMap = mnotes_task_List.get(i);
            //if(((String)myMap.get("name")).contains(Name))
            if(Pattern.compile(Pattern.quote(Name), Pattern.CASE_INSENSITIVE).matcher((String)myMap.get("title")).find())
                return i;
        }
        return 0;
    }


    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//    }
}
